from django import template
from django.db import models
from django.template import loader
from django.shortcuts import render
from django.views import generic
from ..models.honey_question import HoneyQuestion

# Create your views here.
from django.http import HttpResponse

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def details(request, name):
    context = {
        'name': name,

    }
    return render(request, 'home/details.html', context)

class QuestionView(generic.ListView):
    model = HoneyQuestion
    context_object_name = "context"
    template_name = "home/question.html"

    def get_context_data(self) :
        context = {}
        context['name'] = "Long đẹp trai"
        return context