from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.dateparse import parse_datetime
from django.shortcuts import render
from datetime import datetime
from ..models.employee_work_time import EmployeeWorkTime
from django.http import JsonResponse
import json

class EmployeeWorkTimeView:
    @csrf_exempt
    def save(request):
        current_date_time = datetime.now()
        start = request.POST.get('start')
        end = request.POST.get('end')
        type = request.POST.get('type')
        reason = request.POST.get('reason')
        start_date_time = datetime.strptime(start, '%Y/%m/%d %H:%M:%S')
        end_date_time = datetime.strptime(end, '%Y/%m/%d %H:%M:%S')
        work_time = EmployeeWorkTime()
        work_time.start_time = start_date_time
        work_time.end_time = end_date_time
        work_time.off_type = type
        work_time.reason = reason
        work_time.insert_datetime = current_date_time
        work_time.update_datetime = current_date_time
        work_time.save()
        return HttpResponse("Tạo yêu cầu hoàn tất")

    def get_all(request):
        context = {'records' : list(EmployeeWorkTime.objects.values('off_type', 'start_time', 'end_time', 'update_datetime', 'reason'))}
        qr_json = json.dumps(context, ensure_ascii=False, default=str)
        return JsonResponse(qr_json, safe=False)

    def time_off(request):
        context = {}
        return render(request, 'index.html', context)