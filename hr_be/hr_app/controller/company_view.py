from django.shortcuts import render
from django.views import generic
from ..models.company import Company

class CompanyView(generic.TemplateView):
    model = Company()
    template_name = "company/index.html"

    def get_context_data(self, **kwargs):
        company = self.request.GET.get("name")
        context = super().get_context_data(**kwargs)
        context['records'] = Company.objects.all().filter(company_name = company)
        return context
