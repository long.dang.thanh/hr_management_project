from django.shortcuts import render
from ..models.work_calendar import WorkCalendar

class WorkCalendarView():
    def index(request):
        context = {'records' : list(WorkCalendar.objects.all[0: 10]), 'name' : "long"}
        return render(request, "work_calendar/index.html", context)
