from django.shortcuts import render
from ..models.off_type import OffType
from django.http import JsonResponse
import json


class OffTypeView:
    def getAll(request):
        context = {'records' : list(OffType.objects.values('id', 'display'))}
        qr_json = json.dumps(context, ensure_ascii=False, default=str)
        return JsonResponse(qr_json, safe=False)

