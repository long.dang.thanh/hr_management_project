# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class WorkCalendar(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    work_date = models.DateField(db_column='WORK_DATE', blank=True, null=True)  # Field name made lowercase.
    date_type = models.IntegerField(db_column='DATE_TYPE', blank=True, null=True)  # Field name made lowercase.
    group_id = models.IntegerField(db_column='GROUP_ID', blank=True, null=True)  # Field name made lowercase.
    remarks = models.CharField(db_column='REMARKS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    active_flg = models.DecimalField(db_column='ACTIVE_FLG', max_digits=1, decimal_places=0, blank=True, null=True)  # Field name made lowercase.
    insert_datetime = models.DateTimeField(db_column='INSERT_DATETIME', blank=True, null=True)  # Field name made lowercase.
    update_datetime = models.DateTimeField(db_column='UPDATE_DATETIME', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'work_calendar'
