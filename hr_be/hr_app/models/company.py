
from django.db import models

class Company(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    company_name = models.CharField(db_column='COMPANY_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    phone = models.CharField(db_column='PHONE', max_length=20, blank=True, null=True)  # Field name made lowercase.
    fax = models.CharField(db_column='FAX', max_length=20, blank=True, null=True)  # Field name made lowercase.
    web_url = models.CharField(db_column='WEB_URL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    active_flg = models.DecimalField(db_column='ACTIVE_FLG', max_digits=1, decimal_places=0, blank=True, null=True)  # Field name made lowercase.
    insert_datetime = models.DateTimeField(db_column='INSERT_DATETIME')  # Field name made lowercase.
    update_datetime = models.DateTimeField(db_column='UPDATE_DATETIME', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'company'