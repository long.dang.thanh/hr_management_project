from django.urls import path
from .controller import home
from .controller import company_view
from .models.company import Company
from .controller.work_calendar_view import WorkCalendarView
from .controller.off_type_view import OffTypeView
from .controller.employee_work_time_view import EmployeeWorkTimeView

urlpatterns = [
    path('', home.QuestionView.as_view(), name='question'),
    path('off-type', OffTypeView.getAll, name='off-type'),
    path('employee-work-time-save', EmployeeWorkTimeView.save, name='employee-work-time-save'),
    path('employee-work-time-get-all', EmployeeWorkTimeView.get_all, name='employee-work-time-get-all'),
    path('time-off-management', EmployeeWorkTimeView.time_off, name='time-off-management'),
    path('calendar', WorkCalendarView.index, name='calendar_index'),
    path('company', company_view.CompanyView.as_view(model=Company), name='company_index'),
    path('home', home.index, name='index'),
    path('home', home.index, name='index'),
    path('home/<slug:name>/', home.details, name='details'),
]