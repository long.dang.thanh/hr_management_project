import { Button } from 'bootstrap';
import React, { Component, useState, useEffect } from 'react'
import DateTimePicker from 'react-datetime-picker';
import axios from 'axios';
import format from 'date-fns/format';

function TimeOffOption(props) {
    return (<select className="form-control" onChange={props.onChange} >
        {props.types.map((type, i) => {
            return (<option value={type.id}>{type.display}</option>)
        })}
    </select>)
}

function ReasonBox(props) {
    return (
        <div>
            <textarea className="form-control" onChange={props.onChange}>{props.value}</textarea>
            <div>{props.error}</div>
        </div>
    );
}

function TimeOffList(props) {
    const [timeOffItems, setTimeOffItems] = useState([]);

    useEffect(() => {
        fetch("/employee-work-time-get-all")
            .then(res => res.json())
            .then(
                (result) => {
                    result = JSON.parse(result);
                    if (result.hasOwnProperty('records')) {
                        setTimeOffItems(result.records);
                    }
                },
                (error) => {
                    this.setState({
                        items: [],
                        error
                    });
                }
            )
    }, []);

    return (
        <div>
            <table className="table table-trible">
                <thead>
                    <th>Lý do</th>
                    <th>Ngày bắt đầu</th>
                    <th>Ngày kết thúc</th>
                    <th>Chi tết</th>
                    <th>Cập nhật lần cuối</th>
                </thead>
                <tbody>
                    {timeOffItems.map((item, i) => {
                        return (
                            <tr>
                                <td>{item.off_type}</td>
                                <td>{item.start_time}</td>
                                <td>{item.end_time}</td>
                                <td>{item.reason}</td>
                                <td>{item.update_datetime}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    );
}

class DayOffCreator extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 'types': [], type: 1, start: new Date(), end: new Date(), reason: "", error: "" }
        this.fetch = this.fetch.bind(this);
        this.handleFromTimeChange = this.handleFromTimeChange.bind(this);
        this.handleEndTimeChange = this.handleEndTimeChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    componentDidMount() {
        this.fetch();
    }

    fetch() {
        fetch("/off-type")
            .then(res => res.json())
            .then(
                (result) => {
                    result = JSON.parse(result);
                    if (result.hasOwnProperty('records')) {
                        this.setState({ 'types': result.records });
                    }
                },
                (error) => {
                    this.setState({
                        items: [],
                        error
                    });
                }
            )
    }

    handleOffTypeChange(event) {
        this.setState({ type: event.target.value })
    }

    handleFromTimeChange(event) {
        this.setState({ start: event })
    }

    handleEndTimeChange(event) {
        this.setState({ end: event })
    }

    handleReasonChange(event) {
        this.setState({ reason: event.target.value })
    }

    submitForm() {
        if (this.state.reason.trim() == "") {
            this.setState({ error: "Bắt buộc điền lý do" });
            return;
        }
        this.createTimeOff(this.state.start, this.state.end, this.state.type, this.state.reason);
    }

    createTimeOff(start, end, type, reason) {
        const formatedStart = format(start, 'yyyy/MM/dd hh:mm:ss');
        const formatedEnd = format(end, 'yyyy/MM/dd hh:mm:ss');

        const data = new FormData();
        data.append("start", formatedStart);
        data.append("end", formatedEnd);
        data.append("type", type);
        data.append("reason", reason);
        fetch('/employee-work-time-save', {
            method: 'POST',
            body: data
        }).then((response) => {
            if (alert("Đã tạo yêu cầu thành công")) {}
            else  window.location.reload(); 
        });
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-sm-2">
                        Ngày bắt đầu:
                    </div>
                    <div className="col-sm">
                        <DateTimePicker value={this.state.start} onChange={this.handleFromTimeChange} />
                    </div>
                    <div className="col-sm-1"></div>
                    <div className="col-sm-2">
                        Ngày kết thúc
                    </div>
                    <div className="col-sm">
                        <DateTimePicker value={this.state.end} onChange={this.handleEndTimeChange} />
                    </div>
                </div>
                <br></br>
                <div className="col-sm-4">
                    <TimeOffOption types={this.state.types} onChange={this.handleOffTypeChange}></TimeOffOption>
                </div>
                <br></br>
                <ReasonBox value={this.state.reason} onChange={this.handleReasonChange} error={this.state.error} />
                <br></br>
                <button className="btn btn-primary" onClick={this.submitForm} >Tạo Đơn</button>
            </div>
        )
    }
}

function TimeOffManagement() {
    return (
        <div className="container">
            <br></br>
            <DayOffCreator></DayOffCreator>
            <hr></hr>
            <TimeOffList></TimeOffList>
        </div>
    )
}

export default TimeOffManagement;

